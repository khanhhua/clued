REAMDE
======

Cluedo Game Server supports two protocols

- Text based CLI protocol
- VR client tcp protocol

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `clued` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:clued, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/clued](https://hexdocs.pm/clued).

## Run

### Test

```
mix test
```

### Run application

```
iex -S mix
```

## Reference

All rules are based on [Wikipedia#Cluedo](https://en.wikipedia.org/wiki/Cluedo)
