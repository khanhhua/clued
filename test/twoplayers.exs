defmodule TwoPlayers do
  use ExUnit.Case

  setup do
    IO.puts "Setting up...."

    opts = [:binary, packet: :line, active: false]
    {:ok, socket1} = :gen_tcp.connect('localhost', 3838, opts)
    {:ok, socket2} = :gen_tcp.connect('localhost', 3838, opts)

    {:ok, plum: socket1, scarlet: socket2}
  end

  describe "Plum wins" do
    test "Users login without password", %{plum: sockPlum, scarlet: sockScarlet} do
      IO.inspect(sockPlum, label: "Plum")
      IO.inspect(sockScarlet, label: "Scarlet")

      assert send_and_recv(sockPlum, "login khanh") == "OK\n"
      assert send_and_recv(sockScarlet, "login pingu") == "OK\n"
    end

    test "Plum creates game, Scarlet joins", %{plum: sockPlum, scarlet: sockScarlet} do
      raw = send_and_recv(sockPlum, "create plum")
      [status, game_pid] = String.split(String.trim(raw), " ")
      assert status == "OK"
      game_pid_trimmed = String.slice(game_pid, 1, 7)
      assert send_and_recv(sockScarlet, "join #{game_pid_trimmed} scarlet") == "Game can now start\n"
    end


  end

  defp send_and_recv(socket, command) do
    :ok = :gen_tcp.send(socket, command)
    {:ok, data} = :gen_tcp.recv(socket, 0, 1000)
    data
  end
end