defmodule Clued do
  @moduledoc """
  Documentation for Clued.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Clued.hello
      :world

  """
  def hello do
    :world
  end
end
