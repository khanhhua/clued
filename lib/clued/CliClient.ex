defmodule Clued.CliClient do
  require Record
  require Clued.CluedoFSM
  Record.defrecord :server_data, [socket: nil, username: nil, game_pid: nil]

  defguard is_valid_role_as_string(role) when role in ["scarlet", "plum", "peacock", "green", "mustard", "orchid"]
  defguard is_valid_weapon_as_string(weapon) when weapon in ["candlestick", "dagger", "leadpipe", "revolver", "rope", "spanner"]
  defguard is_valid_room_as_string(room) when room in ["kitchen", "ballroom", "conservatory", "billard", "library", "study", "hall", "lounge", "dining"]

  def start_link(client_socket) do
    :gen_statem.start_link(__MODULE__, client_socket, [])
  end

# Mandatory callbacks

  def init(client_socket) do
    Process.flag(:trap_exit, true)
    IO.inspect(client_socket, label: "Client socket")
    :inet.setopts(client_socket, [{:active, true}, {:packet, :raw}, :binary])
    {:ok, :listening, server_data(socket: client_socket)}
  end

  def terminate(reason, _state, _data) do
    IO.puts("Client terminated. Reason: #{reason}")
    :ok
  end

  def code_change(_vsn, state, data, _extra) do
    {:ok, state, data}
  end

  def callback_mode do
    :state_functions
  end

# Public API
  def tell(client_pid, msg) when is_pid(client_pid) do
    IO.inspect client_pid, label: "client_pid"
    IO.puts "Telling \"#{msg}\""
    :gen_statem.cast(client_pid, {:tell, msg})
  end
  def tell(_, _) do
  end

# State and transitions
  def listening(:cast, {:tell, msg}, state) do
    socket = server_data(state, :socket)
    :gen_tcp.send(socket, "#{msg}\n")
    {:keep_state, state}
  end
  def listening(:info, {:tcp_closed, _socket}, state) do
    IO.puts("Socket closed")
    {:stop, :normal, state}
  end

  def listening(:info, {:tcp_error, _socket, reason}, state) do
    IO.puts("Connection closed due to #{reason}")
    {:stop, :normal, state}
  end

  def listening(:info, {:tcp, _socket, cmdline}, state) do
    IO.puts("[listening] Incoming data...")
    IO.inspect(cmdline, label: "cmdline")
    IO.inspect(state, label: "state")

    case parse(String.trim(cmdline)) do
      {:ok, :quit} ->
        {:stop, :normal, state}
      {:ok, command} ->
        IO.inspect command, label: "command"
        {:ok, newState} = apply(__MODULE__, command, [state])
        {:next_state, :listening, newState}
      {:ok, command, args} ->
        IO.inspect command, label: "command"
        IO.inspect args, label: "args"
        {:ok, newState} = apply(__MODULE__, command, [state | args])
        {:next_state, :listening, newState}
      {:error, reason} ->
        IO.inspect(reason, label: "Reason")
        {:next_state, :listening, state}
      any ->
        IO.inspect(any, label: "Some other thing")
        {:next_state, :listening, state}
    end
  end
  def listening(event_type, content, state) do
    IO.inspect event_type, label: "event_type"
    IO.inspect content, label: "content"
    {:keep_state, state}
  end

  @doc """
  Given a command, returns {:ok, :term} if such command exists. If command
  requires parameter(s), parse and return an IOList of arguments also

  ## Examples

    iex> Clued.CliClient.parse(<<"login tom">>)
    {:ok, :login, [<<"tom">>]}

    iex> Clued.CliClient.parse(<<"create plum">>)
    {:ok, :create, [<<"plum">>]}

    iex> Clued.CliClient.parse(<<"start">>)
    {:ok, :start}

    iex> Clued.CliClient.parse(<<"join 111 scarlet">>)
    {:ok, :join, ["111", "scarlet"]}

    iex> Clued.CliClient.parse(<<"spin">>)
    {:ok, :spin}

    iex> Clued.CliClient.parse(<<"guess who what where">>)
    {:ok, :guess, [<<"who">>, <<"what">>, <<"where">>]}

    iex> Clued.CliClient.parse(<<"conclude who what where">>)
    {:ok, :conclude, [<<"who">>, <<"what">>, <<"where">>]}

    iex> Clued.CliClient.parse(<<"pass">>)
    {:ok, :pass}

    iex> Clued.CliClient.parse(<<"bogus">>)
    {:error, :invalidcmd}

  """
  def parse(<<"login", arg :: binary>>) do
    {:ok, :login, [String.trim(arg)]}
  end
  def parse(<<"create", arg :: binary>>) do
    args = Enum.map(String.split(String.trim(arg), <<" ">>) , &(&1))
    if length(args) == 1 do
      {:ok, :create, args}
    else
      {:error, :invalidargs}
    end
  end
  def parse(<<"start">>) do
    {:ok, :start}
  end
  def parse(<<"join", arg :: binary>>) do
    args = Enum.map(String.split(String.trim(arg), <<" ">>) , &(&1))
    if length(args) == 2 do
      {:ok, :join, args}
    else
      {:error, :invalidargs}
    end
  end
  def parse(<<"spin">>) do
    {:ok, :spin}
  end
  def parse(<<"move", arg :: binary>>) do
    {:ok, :move, [String.trim(arg)]}
  end
  def parse(<<"guess", arg :: binary>>) do
    args = Enum.map(String.split(String.trim(arg), <<" ">>) , &(&1))
    if length(args) == 3 do
      {:ok, :guess, args}
    else
      {:error, :invalidargs}
    end
  end
  def parse(<<"pass">>) do
    {:ok, :pass}
  end
  def parse(<<"conclude", arg :: binary>>) do
    args = Enum.map(String.split(String.trim(arg), <<" ">>) , &(&1))
    if length(args) == 3 do
      {:ok, :conclude, args}
    else
      {:error, :invalidargs}
    end
  end
  def parse(<<"status">>) do
    {:ok, :status}
  end
  def parse(_anything) do
    {:error, :invalidcmd}
  end

  # CLI binding handlers

  def login(state, username) do
    IO.puts "Logging in as #{username}"
    tell(self(), "OK")

    {:ok, server_data(state, username: username)}
  end

  def create(state, role)
  when is_valid_role_as_string(role) do
    username = server_data(state, :username)
    IO.puts "[#{username}] Creating new Cluedo game"
    game_id = :os.system_time(:millisecond)
    try do
      {:ok, game_pid} = Clued.CluedoFSM.start_link(game_id)
      IO.inspect game_pid, label: "game_pid"
      tell(self(), "OK #{:erlang.pid_to_list(game_pid)}")
      :ok = Clued.CluedoFSM.join_game(game_pid, String.to_atom(role))
      {:ok, server_data(state, game_pid: game_pid)}
    rescue
      e ->
        IO.inspect e, "error"
        {:ok, state}
    end
  end

  def join(state, game_pid, role)
  when server_data(state, :game_pid) == nil and is_valid_role_as_string(role) do
    username = server_data(state, :username)
    IO.puts "[#{username}] Joining Cluedo game <#{game_pid}> as role #{role}"
    pid = :erlang.list_to_pid('<#{game_pid}>')
    :ok = Clued.CluedoFSM.join_game(pid, String.to_atom(role))
    tell(self(), "OK")
    {:ok, server_data(state, game_pid: pid)}
  rescue
    e ->
      username = server_data(state, :username)
      IO.inspect e, label: "[#{username}] Error"
      {:ok, state}
  end

  def start(state)
  when server_data(state, :game_pid) != nil do
    game_pid = server_data(state, :game_pid)
    username = server_data(state, :username)

    IO.inspect game_pid, label: "[#{username}] game_pid"
    IO.puts "[#{username}] Starting Cluedo game"
    :ok = Clued.CluedoFSM.start_game(game_pid)
    IO.puts "[#{username}] Game started"
    {:ok, state}
  rescue
    e ->
      username = server_data(state, :username)
      IO.inspect e, label: "[#{username}] Error"
      {:ok, state}
  end

  def spin(state) when server_data(state, :game_pid) != nil
  do
    game_pid = server_data(state, :game_pid)
    username = server_data(state, :username)

    IO.inspect game_pid, label: "[#{username}] game_pid"
    Clued.CluedoFSM.spin(game_pid)

    {:ok, state}
  rescue
    e ->
      IO.inspect e, label: "Error"
      {:ok, state}
  end

  def move(state, location) do
    username = server_data(state, :username)
    game_pid = server_data(state, :game_pid)
    {:ok, active} = Clued.CluedoFSM.active_player(game_pid)
    if active != self() do
      IO.puts "[#{username}] Cannot move"
    else
      IO.puts "[#{username}] Moving to location #{location}"
      Clued.CluedoFSM.move(game_pid, location)
      {:ok, state}
    end
  rescue
    e ->
      IO.inspect e, label: "Error"
      {:ok, state}
  end

  def guess(state, who, what, where)
  when server_data(state, :game_pid) != nil
      and is_valid_role_as_string(who)
      and is_valid_weapon_as_string(what)
      and is_valid_room_as_string(where)
  do
    username = server_data(state, :username)
    game_pid = server_data(state, :game_pid)
    IO.puts "[#{username}] Guessing #{who} used the #{what} in #{where}"

    suspect = String.to_atom(who)
    weapon = String.to_atom(what)
    scene = String.to_atom(where)
    Clued.CluedoFSM.guess(game_pid, suspect, weapon, scene)

    {:ok, state}
  rescue
    e ->
      IO.inspect e, label: "Error"
      {:ok, state}
  end
  def guess(state, who, what, where) do
    username = server_data(state, :username)
    IO.puts "[#{username}] Invalid guess (#{who}, #{what}, #{where})"
    {:ok, state}
  end

  def pass(state)
  when server_data(state, :game_pid) != nil do
    username = server_data(state, :username)
    game_pid = server_data(state, :game_pid)
    IO.puts "[#{username}] Passing to the next player"

    Clued.CluedoFSM.pass(game_pid)
    {:ok, state}
  end

  def conclude(state, who, what, where)
  when server_data(state, :game_pid) != nil
      and is_valid_role_as_string(who)
      and is_valid_weapon_as_string(what)
      and is_valid_room_as_string(where)
  do
    username = server_data(state, :username)
    game_pid = server_data(state, :game_pid)

    IO.puts "[#{username}] Concluding #{who} used the #{what} in #{where}"

    suspect = String.to_atom(who)
    weapon = String.to_atom(what)
    scene = String.to_atom(where)

    Clued.CluedoFSM.conclude(game_pid, suspect, weapon, scene)
    {:ok, state}
  end
  def conclude(state, who, what, where) do
    username = server_data(state, :username)
    IO.puts "[#{username}] Invalid conclusion (#{who}, #{what}, #{where})"
    {:ok, state}
  end

  def status(state) do
    username = server_data(state, :username)
    game_pid = server_data(state, :game_pid)
    IO.puts "[#{username}] Query for player status"

    {:ok, status} = Clued.CluedoFSM.status(game_pid)
    IO.inspect status, label: "Status"

    location = :proplists.get_value(:location, status)
    cards = :proplists.get_value(:cards, status)
      |> Enum.map_join(", ", &(Clued.CluedoFSM.card(&1, :content)))
    tell(self(),
      """
      **********************************************
      Location: #{location}
      Cards: #{cards}
      **********************************************
      """)

    {:ok, state}
  rescue
    e ->
      IO.inspect e, label: "Error"
      {:ok, state}
  end
end
