defmodule Clued.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    ip = Application.get_env(:clued, :ip, {127,0,0,1})
    port = Application.get_env(:clued, :cli_port, 3838)

    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: Clued.Worker.start_link(arg)
      {Clued.CliServer, [ip, port]},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Clued.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
