defmodule Clued.CluedoFSM do
  require Logger
  @behaviour :gen_statem

  @moduledoc """

  CluedoFSM is an state machine for a session of Cluedo game.
  Transition table:
  - Awaiting players:

  - First spin:
    Player roles the dice to decide who should go first, second, third
    When the order has been determined, system decide at random the killer, the
    weapon and the room.
    Active player is the first on the player order list
    Cards are distributed evenly (3 to each player)

  - Playing in the hall:
    - Dice: Active user roles the dice to determine how far he can go
    - Move: User makes a move in CW or CCW direction.
      If player can reach a room entrance, he will enter that room
      If player cannot reach a room, the next player will take his turn
  - Playing in a rooom:
    - Guess: User makes a guess by naming the suspect, the weapon and the room.
      The suspect will be summoned to the room
      System will show which person has the card
    - Conclude: Make a conclusion
      If the conclusion matches the crime, active player is the winner
      If not, active player is declared "DONE" and cannot make further moves

  """

  @type player_role :: :scarlet | :plum | :peacock | :green | :mustard | :orchid
  @type crime_weapon :: :candlestick | :dagger | :leadpipe | :revolver | :rope | :spanner
  @type crime_scene :: :kitchen | :ballroom | :conservatory | :billard | :library | :study | :hall | :dining | :lounge
  @type card_type :: :role | :weapon | :room | :action

  require Record

  Record.defrecord :card, [type: nil, content: nil]
  Record.defrecord :crime, [who: nil, what: nil, where: nil]
  Record.defrecord :player, [name: nil, location: nil, last_location: nil, cards: nil, done: false, pid: nil]
  Record.defrecord :cell, [location: nil, entrance: nil]
  Record.defrecord :guess, [who: nil, what: nil, where: nil]
  Record.defrecord :game_state, [
    can_start: false,
    active: nil, # the active player
    crime: nil,
    players: nil,
    floorplan: nil,
    player_order: nil, # spinning determines the order of players
    last_spin: nil,
    winner: nil, # the winner
    room: nil, # the room scene active player is in
    guess: nil # the guess guess by the active player
  ]

  defguard is_valid_role(role) when role in [:scarlet, :plum, :peacock, :green, :mustard, :orchid]
  defguard is_valid_room(room) when room in [:kitchen, :ballroom, :conservatory, :billard, :library, :study, :hall, :dining, :lounge]

  def start_link(game_id) do
    # IO.inspect game_id, label: "CluedoFSM - start link"
    :gen_statem.start_link(__MODULE__, game_id, [])
  end

  # Mandatory callbacks
  def init(_game_id) do
    state = game_state(
      players: %{
        scarlet: player(name: "Miss Scarlett", location: :cell_e4, pid: nil),
        plum: player(name: "Professor Plum", location: :cell_g4, pid: nil),
        peacock: player(name: "Mrs. Peacock", location: :cell_i4, pid: nil),
        green: player(name: "Reverend Mr Green", location: :cell_g7, pid: nil),
        mustard: player(name: "Colonel Mustard", location: :cell_e7, pid: nil),
        orchid: player(name: "Dr. Orchid", location: :cell_d5, pid: nil)
      },
      # TODO Load floor plan and traversability function from file
      floorplan: [
        # Let's make a simple squarish hall where people can only walk along the
        # edge of the hall, along the wall.
        # refer to the floorplan file floors.numbers for cell location identifiers
        #
        # Only adjacent cells are traversible. Also the first and the last are
        # linked.
        cell(location: :cell_d4, entrance: :kitchen),
        cell(location: :cell_e4),
        cell(location: :cell_f4, entrance: :ballroom),
        cell(location: :cell_g4),
        cell(location: :cell_h4, entrance: :conservatory),
        cell(location: :cell_i4),
        cell(location: :cell_i5, entrance: :billard),
        cell(location: :cell_i6, entrance: :library),
        cell(location: :cell_i7),
        cell(location: :cell_h7, entrance: :study),
        cell(location: :cell_g7),
        cell(location: :cell_f7, entrance: :hall),
        cell(location: :cell_e7),
        cell(location: :cell_d7, entrance: :lounge),
        cell(location: :cell_d6, entrance: :dining),
        cell(location: :cell_d5) # circular back to cell_d4
      ])
    {:ok, :awaiting_players, state}
  end

  def terminate(reason, _state, _data) do
    Logger.warn "Game terminated. Reason: #{reason}"
    :ok
  end

  def code_change(_vsn, state, data, _extra) do
    {:ok, state, data}
  end

  def callback_mode do
    :state_functions
  end

  # Public API for game logic
  def active_player(game_pid) do
    :gen_statem.call(game_pid, {:active_player})
  end

  def status(game_pid) do
    :gen_statem.call(game_pid, {:status})
  end

  @spec join_game(pid, player_role) :: term
  def join_game(game_pid, player_role) when is_valid_role(player_role) do
    player_pid = self()
    :gen_statem.call(game_pid, {:join_game, player_pid, player_role})
  end

  def start_game(game_pid) do
    :gen_statem.call(game_pid, {:start_game, []})
  end

  def spin(game_pid) do
    :gen_statem.call(game_pid, {:spin})
  end

  def move(game_pid, location) do
    :gen_statem.call(game_pid, {:move, location})
  end

  @spec guess(pid, player_role, crime_weapon, crime_scene) :: term
  def guess(game_pid, suspect, weapon, scene) do
    :gen_statem.call(game_pid, {:guess, suspect, weapon, scene})
  end

  def pass(game_pid) do
    :gen_statem.call(game_pid, {:pass})
  end

  @spec conclude(pid, player_role, crime_weapon, crime_scene) :: term
  def conclude(game_pid, suspect, weapon, scene) do
    :gen_statem.call(game_pid, {:conclude, suspect, weapon, scene})
  end
  # Public game state

  def handle_common({:call, from_ref}, {:active_player}, state) do
    {:keep_state, state, [{:reply, from_ref, {:ok, game_state(state, :active)}}]}
  end
  def handle_common({:call, from_ref}, {:status}, state) do
    {player_pid, _} = from_ref
    {_, p} = Enum.find(game_state(state, :players), fn {_, it} ->
      player(it, :pid) == player_pid
    end)

    active = game_state(state, :active)
    location = player(p, :location)
    cards = player(p, :cards) || []
    {:keep_state, state, [{:reply, from_ref, {:ok, [
      active: active,
      location: location,
      cards: cards
    ]}}]}
  end
  def handle_common({:call, from_ref}, content, state) do
    Logger.debug "unkown method. No-op."
    Logger.debug inspect([content: content])
    {:keep_state, state, [{:reply, from_ref, :ok}]}
  end
  @doc """
  Given the state of the game is awaiting_players, update the list of players
  when a new player joins as a certain role

  ## Examples

  iex> Clued.CluedoFSM.awaiting_players({:call, {0,0}}, {:join_game, :pid, :scarlet},
  iex>   Clued.CluedoFSM.game_state(
  iex>      players: %{scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :unknown, pid: nil)}
  iex>   ))
  {:next_state, :awaiting_players,
    Clued.CluedoFSM.game_state(
      players: %{scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :unknown, pid: :pid)}
    ),
    [{:reply, 0, :ok}]}

  iex> Clued.CluedoFSM.awaiting_players({:call, {0,0}, {:join_game, :pid_new, :scarlet},
  iex>   Clued.CluedoFSM.game_state(
  iex>     players: %{scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :unknown, pid: :pid)}
  iex>   ))
  {:next_state, :awaiting_players,
    Clued.CluedoFSM.game_state(
      players: %{scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :unknown, pid: :pid)}
    ),
    [{:reply, 0, {:error, :roletaken}}]}

  """
  def awaiting_players({:call, from_ref}, {:join_game, player_pid, player_role}, state) do
    # IO.inspect(from_ref, label: "from_ref")
    # IO.inspect(player_pid, label: "player_pid")

    players = game_state(state, :players)
    existing_player = players[player_role]
    if player(existing_player, :pid) != nil do
      Logger.error "Role has been taken"
      Clued.CliClient.tell(player_pid, "Role has been taken")
      {:next_state, :awaiting_players, state, [{:reply, from_ref, {:error, :roletaken}}]}
    else
      Logger.info "Taking on role #{player_role}"
      new_players = Map.replace!(players, player_role, player(existing_player, pid: player_pid))
      new_game_state = game_state(state, players: new_players)

      if is_enough_players(new_players) == true do
        Clued.CliClient.tell(player_pid, "Game can now start")
        {:next_state, :awaiting_players, game_state(new_game_state, can_start: true),
          [{:reply, from_ref, :ok}]
        }
      else
        Clued.CliClient.tell(player_pid, "Awaiting more players...")
        {:next_state, :awaiting_players, new_game_state,
          [{:reply, from_ref, :ok}]
        }
      end
    end
  end

  @doc """
  Given that the game is awaiting players, game host can start the game if the
  number of players suffices (2 or above)

  ## Examples

  iex> Clued.CluedoFSM.awaiting_players({:call, {0,0}}, {:start_game, []},
  iex>   Clued.CluedoFSM.game_state(
  iex>     can_start: true,
  iex>     players: %{
  iex>       scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :unknown, pid: :pid_scarlet),
  iex>       plum: Clued.CluedoFSM.player(name: "Professor Plum", location: :unknown, pid: :pid_plum)
  iex>     }))
  {:next_state, :first_spin,
      Clued.CluedoFSM.game_state(
        can_start: true,
        players: %{
          scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :unknown, pid: :pid_scarlet),
          plum: Clued.CluedoFSM.player(name: "Professor Plum", location: :unknown, pid: :pid_plum)
        }
      ),
     [{:reply, {0, 0}, :ok}]}

  """
  def awaiting_players({:call, from_ref}, {:start_game, []}, state) do
    {player_pid, _} = from_ref
    Logger.info "Attempting to start game"
    if game_state(state, :can_start) == false do
      Clued.CliClient.tell(player_pid, "Awaiting more players...")
      {:next_state, :awaiting_players, state, [{:reply, from_ref, {:error, :playerscount}}]}
    else
      Logger.info "Starting game"
      Clued.CliClient.tell(player_pid, "Starting game")
      {:next_state, :first_spin, state, [{:reply, from_ref, :ok}]}
    end
  end
  def awaiting_players(event_type, content, state) do
    handle_common(event_type, content, state)
  end

  @doc """
  Given the game is in "first_spin" state, user will continue to spin until all
  players take a spin

  iex> :rand.seed(:exsplus, {1, 2, 3})
  iex> Clued.CluedoFSM.first_spin({:call, {:pid_scarlet, 1}}, {:spin},
  iex>   Clued.CluedoFSM.game_state(
  iex>     can_start: true,
  iex>     players: %{
  iex>       scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :unknown, pid: :pid_scarlet),
  iex>       plum: Clued.CluedoFSM.player(name: "Professor Plum", location: :unknown, pid: :pid_plum)
  iex>     }))
  {:next_state, :first_spin,
    Clued.CluedoFSM.game_state(
      can_start: true,
      players: %{
        scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :unknown, pid: :pid_scarlet),
        plum: Clued.CluedoFSM.player(name: "Professor Plum", location: :unknown, pid: :pid_plum)
      },
      player_order: [{:pid_scarlet, 6}]
    ),
    [{:reply, {:pid_scarlet, 1}, {:ok, 6}}]}

  iex> :rand.seed(:exsplus, {1, 21, 3})
  iex> Clued.CluedoFSM.first_spin({:call, {:pid_plum, 2}}, {:spin},
  iex>   Clued.CluedoFSM.game_state(
  iex>     player_order: [{:pid_scarlet, 6}],
  iex>     can_start: true,
  iex>     players: %{
  iex>       scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :unknown, pid: :pid_scarlet),
  iex>       plum: Clued.CluedoFSM.player(name: "Professor Plum", location: :unknown, pid: :pid_plum)
  iex>     }))
  {:next_state, :playing_hall,
    Clued.CluedoFSM.game_state(
      can_start: true,
      crime: Clued.CluedoFSM.crime(who: :plum, what: :spanner, where: :conservatory),
      players: %{
        scarlet: Clued.CluedoFSM.player(
          name: "Miss Scarlett",
          location: :unknown,
          pid: :pid_scarlet,
          cards: [
            Clued.CluedoFSM.card(type: :who, content: :orchid),
            Clued.CluedoFSM.card(type: :weapon, content: :candlestick),
            Clued.CluedoFSM.card(type: :who, content: :scarlet)
          ]),
        plum: Clued.CluedoFSM.player(
          name: "Professor Plum",
          location: :unknown,
          pid: :pid_plum,
          cards: [
            Clued.CluedoFSM.card(type: :room, content: :study),
            Clued.CluedoFSM.card(type: :who, content: :peacock),
            Clued.CluedoFSM.card(type: :weapon, content: :dagger)
          ])
      },
      player_order: [
        {:pid_plum, 7},
        {:pid_scarlet, 6}
      ],
      active: :pid_plum
    ),
    [{:reply, {:pid_plum, 2}, {:ok, 7}}]}

  """
  def first_spin({:call, from_ref}, {:spin}, state) do
    Logger.info "Spin first round for player order"
    {player_pid, _} = from_ref
    case game_state(state, :player_order) do
      nil ->
        dice = spin_dice()
        Logger.debug inspect([dice: dice])
        Clued.CliClient.tell(player_pid, "You've rolled #{dice}")

        new_game_state = game_state(state, player_order: [{player_pid, dice}])
        {:next_state, :first_spin, new_game_state, [{:reply, from_ref, {:ok, dice}}]}
      player_order ->
        dice = spin_dice()
        Logger.debug inspect([dice: dice])
        Clued.CliClient.tell(player_pid, "You've rolled #{dice}")

        new_player_order = Enum.reverse(List.keysort([{player_pid, dice} | player_order], 1))
        count = length(new_player_order)
        count_active_players = length(Enum.filter(game_state(state, :players), fn {_role, p} ->
          player(p, :pid) != nil
        end))

        Logger.debug inspect([new_player_order: new_player_order,
                              count_active_players: count_active_players])

        if count == count_active_players do
          [{active, _}| _] = new_player_order
          [ crime_cards | player_deck ] = deal_deck(7)
          # IO.inspect crime_cards, label: "crime_cards"
          # IO.inspect player_deck, label: "player_deck"

          players = Enum.map(game_state(state, :players), &(&1))
          {_, new_players} = Enum.reduce(players, {0, game_state(state, :players)}, fn {role, p}, {index, acc} ->
            {index + 1, Map.replace!(acc, role, player(p, cards: Enum.at(player_deck, index)))}
          end)

          if active == player_pid do
            Clued.CliClient.tell(player_pid, "It's your move")
          end

          new_game_state = game_state(state,
            player_order: new_player_order,
            players: new_players,
            active: active,
            crime: generate_crime(crime_cards))
          {:next_state, :playing_hall, new_game_state, [{:reply, from_ref, {:ok, dice}}]}
        else
          new_game_state = game_state(state, player_order: new_player_order)
          {:next_state, :first_spin, new_game_state, [{:reply, from_ref, {:ok, dice}}]}
        end
    end
  end
  def first_spin(event_type, content, state) do
    handle_common(event_type, content, state)
  end

  @doc """
  Given the state is playing_hall, player can make a spin, last_spin should be
  updated

  ## Examples

  iex> :rand.seed(:exsplus, {1, 2, 3})
  iex> Clued.CluedoFSM.playing_hall({:call, {:pid_scarlet, 1}}, {:spin},
  iex>   Clued.CluedoFSM.game_state(
  iex>     active: :pid_scarlet,
  iex>     can_start: true,
  iex>     player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
  iex>     players: %{
  iex>       scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :unknown, pid: :pid_scarlet),
  iex>       plum: Clued.CluedoFSM.player(name: "Professor Plum", location: :unknown, pid: :pid_plum)
  iex>     }))
  {:next_state, :playing_hall,
    Clued.CluedoFSM.game_state(
       active: :pid_scarlet,
       last_spin: 6,
       can_start: true,
       player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
       players: %{
         scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :unknown, pid: :pid_scarlet),
         plum: Clued.CluedoFSM.player(name: "Professor Plum", location: :unknown, pid: :pid_plum)
       }), [{:reply, {:pid_scarlet, 1}, :ok}]}

  """
  def playing_hall({:call, from_ref}, {:spin}, state)
  when game_state(state, :last_spin) != nil do
    Logger.error "Subsequent call(s) to spin"
    {player_pid, _}  = from_ref
    Clued.CliClient.tell(player_pid, "You have already rolled the dice")
    {:keep_state, state, [{:reply, from_ref, {:error, :badcall}}]}
  end
  def playing_hall({:call, from_ref}, {:spin}, state)
  when elem(from_ref, 0) != game_state(state, :active) do
    Logger.error "Not the active player"
    {player_pid, _}  = from_ref
    Clued.CliClient.tell(player_pid, "You are not the active player")
    {:keep_state, state, [{:reply, from_ref, {:error, :badcall}}]}
  end
  def playing_hall({:call, from_ref}, {:spin}, state) do
    {player_pid, _} = from_ref
    Logger.debug inspect([player_pid: player_pid])

    dice = spin_dice()
    Clued.CliClient.tell(player_pid, "You've rolled #{dice}")

    Logger.debug inspect([dice: dice])
    new_game_state = game_state(state, last_spin: dice)

    {:next_state, :playing_hall, new_game_state, [{:reply, from_ref, {:ok, dice}}]}
  end

  @doc """
  Given that player has roled the dice in the hall, he should be able to make a move

  TODO: Location should be binary as they should be considered external resource

  ## Examples

  iex> :rand.seed(:exsplus, {1, 2, 3})
  iex> floorplan = [
  iex>   Clued.CluedoFSM.cell(location: :a1),
  iex>   Clued.CluedoFSM.cell(location: :a2),
  iex>   Clued.CluedoFSM.cell(location: :a3),
  iex>   Clued.CluedoFSM.cell(location: :a4),
  iex>   Clued.CluedoFSM.cell(location: :a5),
  iex>   Clued.CluedoFSM.cell(location: :a6),
  iex>   Clued.CluedoFSM.cell(location: :a7)
  iex> ]
  iex> Clued.CluedoFSM.playing_hall({:call, {:pid_scarlet, 1}}, {:move, :a2},
  iex>   Clued.CluedoFSM.game_state(
  iex>     active: :pid_scarlet,
  iex>     last_spin: 6,
  iex>     can_start: true,
  iex>     player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
  iex>     floorplan: floorplan,
  iex>     players: %{
  iex>       scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :a1, pid: :pid_scarlet),
  iex>       plum: Clued.CluedoFSM.player(name: "Professor Plum", location: :unknown, pid: :pid_plum)
  iex>     }))
  {:next_state, :playing_hall,
    Clued.CluedoFSM.game_state(
       active: :pid_scarlet,
       last_spin: 6,
       can_start: true,
       player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
       floorplan: [
         Clued.CluedoFSM.cell(location: :a1),
         Clued.CluedoFSM.cell(location: :a2),
         Clued.CluedoFSM.cell(location: :a3),
         Clued.CluedoFSM.cell(location: :a4),
         Clued.CluedoFSM.cell(location: :a5),
         Clued.CluedoFSM.cell(location: :a6),
         Clued.CluedoFSM.cell(location: :a7)
       ],
       players: %{
         scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :a2, last_location: :a1, pid: :pid_scarlet),
         plum: Clued.CluedoFSM.player(name: "Professor Plum", location: :unknown, pid: :pid_plum)
       }), [{:reply, {:pid_scarlet, 1}, :ok}]}

  """
  def playing_hall({:call, from_ref}, {:move, location}, state) when is_bitstring(location) do
    {player_pid, _} = from_ref
    floorplan = game_state(state, :floorplan)

    if !is_valid_location(floorplan, location) do
      Clued.CliClient.tell(player_pid, "Cannot move! #{location} is invalid")
      {:next_state, :playing_hall, state, [{:reply, from_ref, {:error, :badcall}}]}
    else
      playing_hall({:call, from_ref}, {:move, String.to_atom(location)}, state)
    end
  end
  def playing_hall({:call, from_ref}, {:move, location}, state) when is_atom(location) do
    {player_pid, _} = from_ref
    {role, p} = Enum.find(game_state(state, :players), nil, fn({_key, it}) ->
        player(it, :pid) == player_pid
    end)
    if p == nil do
      {:next_state, :playing_hall, state, [{:reply, from_ref, {:error, :badcall}}]}
    else
      case move_to_location(state, p, role, location) do
        {:error, err} ->
          {:next_state, :playing_hall, state, [{:reply, from_ref, {:error, err}}]}
        {:playing_hall, new_game_state} ->
          {:next_state, :playing_hall, new_game_state, [{:reply, from_ref, :ok}]}
        {:playing_room, new_game_state} ->
          {:next_state, :playing_room, new_game_state, [{:reply, from_ref, :ok}]}
      end
    end
  end
  def playing_hall(event_type, content, state) do
    handle_common(event_type, content, state)
  end

  @doc """
  Given that player has roled the dice in the room, he should be able to make a move
  Location should be binary as they should be considered external resource

  Given the player has made a guess, he can pass his turn on the next player

  ## Examples
  iex> :rand.seed(:exsplus, {1, 2, 3})
  iex> floorplan = [
  iex>   Clued.CluedoFSM.cell(location: :a1, entrance: :kitchen),
  iex>   Clued.CluedoFSM.cell(location: :a2),
  iex>   Clued.CluedoFSM.cell(location: :a3),
  iex>   Clued.CluedoFSM.cell(location: :a4, entrance: :study),
  iex>   Clued.CluedoFSM.cell(location: :a5),
  iex>   Clued.CluedoFSM.cell(location: :a6),
  iex>   Clued.CluedoFSM.cell(location: :a7)
  iex> ]
  iex> Clued.CluedoFSM.playing_room({:call, {:pid_scarlet, 1}}, {:guess, :plum, :rope, :kitchen},
  iex>   Clued.CluedoFSM.game_state(
  iex>     active: :pid_scarlet,
  iex>     last_spin: 6,
  iex>     can_start: true,
  iex>     player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
  iex>     floorplan: floorplan,
  iex>     players: %{
  iex>       scarlet: Clued.CluedoFSM.player(
  iex>          name: "Miss Scarlett",
  iex>          location: :kitchen,
  iex>          pid: :pid_scarlet,
  iex>          cards: [
  iex>            Clued.CluedoFSM.card(type: :who, content: :orchid),
  iex>            Clued.CluedoFSM.card(type: :weapon, content: :candlestick),
  iex>            Clued.CluedoFSM.card(type: :who, content: :scarlet)
  iex>          ]),
  iex>       plum: Clued.CluedoFSM.player(
  iex>          name: "Professor Plum",
  iex>          location: :unknown,
  iex>          pid: :pid_plum,
  iex>          cards: [
  iex>            Clued.CluedoFSM.card(type: :room, content: :study),
  iex>            Clued.CluedoFSM.card(type: :who, content: :peacock),
  iex>            Clued.CluedoFSM.card(type: :weapon, content: :dagger)
  iex>          ])
  iex>     },
  iex>     room: :kitchen,
  iex>     crime: Clued.CluedoFSM.crime(who: :plum, what: :rope, where: :kitchen)))
  {:next_state, :playing_room,
    Clued.CluedoFSM.game_state(
       active: :pid_scarlet,
       last_spin: 6,
       can_start: true,
       player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
       floorplan: [
         Clued.CluedoFSM.cell(location: :a1, entrance: :kitchen),
         Clued.CluedoFSM.cell(location: :a2),
         Clued.CluedoFSM.cell(location: :a3),
         Clued.CluedoFSM.cell(location: :a4, entrance: :study),
         Clued.CluedoFSM.cell(location: :a5),
         Clued.CluedoFSM.cell(location: :a6),
         Clued.CluedoFSM.cell(location: :a7)
       ],
       players: %{
         scarlet: Clued.CluedoFSM.player(
           name: "Miss Scarlett",
           location: :kitchen,
           pid: :pid_scarlet,
           cards: [
             Clued.CluedoFSM.card(type: :who, content: :orchid),
             Clued.CluedoFSM.card(type: :weapon, content: :candlestick),
             Clued.CluedoFSM.card(type: :who, content: :scarlet)
           ]
         ),
         plum: Clued.CluedoFSM.player(
           name: "Professor Plum",
           location: :kitchen,
           last_location: :kitchen,
           pid: :pid_plum,
           cards: [
             Clued.CluedoFSM.card(type: :room, content: :study),
             Clued.CluedoFSM.card(type: :who, content: :peacock),
             Clued.CluedoFSM.card(type: :weapon, content: :dagger)]
         )
       },
       room: :kitchen,
       guess: Clued.CluedoFSM.guess(who: :plum, what: :rope, where: :kitchen),
       crime: Clued.CluedoFSM.crime(who: :plum, what: :rope, where: :kitchen)
    ), [{:reply, {:pid_scarlet, 1}, :ok}]}

  iex> floorplan = [
  iex>   Clued.CluedoFSM.cell(location: :a1, entrance: :kitchen),
  iex>   Clued.CluedoFSM.cell(location: :a2),
  iex>   Clued.CluedoFSM.cell(location: :a3),
  iex>   Clued.CluedoFSM.cell(location: :a4, entrance: :study),
  iex>   Clued.CluedoFSM.cell(location: :a5),
  iex>   Clued.CluedoFSM.cell(location: :a6),
  iex>   Clued.CluedoFSM.cell(location: :a7)
  iex> ]
  iex> Clued.CluedoFSM.playing_room({:call, {:pid_scarlet, 0}}, {:pass},
  iex>   Clued.CluedoFSM.game_state(
  iex>     active: :pid_scarlet,
  iex>     last_spin: 6,
  iex>     can_start: true,
  iex>     player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
  iex>     floorplan: floorplan,
  iex>     players: %{
  iex>       scarlet: Clued.CluedoFSM.player(
  iex>          name: "Miss Scarlett",
  iex>          location: :kitchen,
  iex>          pid: :pid_scarlet,
  iex>          cards: [
  iex>            Clued.CluedoFSM.card(type: :who, content: :orchid),
  iex>            Clued.CluedoFSM.card(type: :weapon, content: :candlestick),
  iex>            Clued.CluedoFSM.card(type: :who, content: :scarlet)
  iex>          ]),
  iex>       plum: Clued.CluedoFSM.player(
  iex>          name: "Professor Plum",
  iex>          location: :study,
  iex>          pid: :pid_plum,
  iex>          cards: [
  iex>            Clued.CluedoFSM.card(type: :room, content: :study),
  iex>            Clued.CluedoFSM.card(type: :who, content: :peacock),
  iex>            Clued.CluedoFSM.card(type: :weapon, content: :dagger)
  iex>          ])
  iex>     },
  iex>     room: :kitchen,
  iex>     guess: Clued.CluedoFSM.guess(who: :plum, what: :rope, where: :kitchen)
  iex>   ))
  {:next_state, :playing_room,
    Clued.CluedoFSM.game_state(
       active: :pid_plum,
       can_start: true,
       player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
       floorplan: [
         Clued.CluedoFSM.cell(location: :a1, entrance: :kitchen),
         Clued.CluedoFSM.cell(location: :a2),
         Clued.CluedoFSM.cell(location: :a3),
         Clued.CluedoFSM.cell(location: :a4, entrance: :study),
         Clued.CluedoFSM.cell(location: :a5),
         Clued.CluedoFSM.cell(location: :a6),
         Clued.CluedoFSM.cell(location: :a7)
       ],
       players: %{
         scarlet: Clued.CluedoFSM.player(
           name: "Miss Scarlett",
           location: :kitchen,
           pid: :pid_scarlet,
           cards: [
             Clued.CluedoFSM.card(type: :who, content: :orchid),
             Clued.CluedoFSM.card(type: :weapon, content: :candlestick),
             Clued.CluedoFSM.card(type: :who, content: :scarlet)
           ]),
           plum: Clued.CluedoFSM.player(
             name: "Professor Plum",
             location: :study,
             pid: :pid_plum,
             cards: [
               Clued.CluedoFSM.card(type: :room, content: :study),
               Clued.CluedoFSM.card(type: :who, content: :peacock),
               Clued.CluedoFSM.card(type: :weapon, content: :dagger)
             ])
       },
       room: :kitchen
     ), [{:reply, {:pid_scarlet, 0}, :ok}]}

  iex> :rand.seed(:exsplus, {1, 2, 660})
  iex> floorplan = [
  iex>   Clued.CluedoFSM.cell(location: :a1, entrance: :kitchen),
  iex>   Clued.CluedoFSM.cell(location: :a2),
  iex>   Clued.CluedoFSM.cell(location: :a3),
  iex>   Clued.CluedoFSM.cell(location: :a4, entrance: :study),
  iex>   Clued.CluedoFSM.cell(location: :a5),
  iex>   Clued.CluedoFSM.cell(location: :a6),
  iex>   Clued.CluedoFSM.cell(location: :a7)
  iex> ]
  iex> Clued.CluedoFSM.playing_room({:call, {:pid_scarlet, 1}}, {:move, :a2},
  iex>   Clued.CluedoFSM.game_state(
  iex>     active: :pid_scarlet,
  iex>     last_spin: 6,
  iex>     can_start: true,
  iex>     player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
  iex>     floorplan: floorplan,
  iex>     players: %{
  iex>       scarlet: Clued.CluedoFSM.player(
  iex>          name: "Miss Scarlett",
  iex>          location: :kitchen,
  iex>          pid: :pid_scarlet,
  iex>          cards: [
  iex>            Clued.CluedoFSM.card(type: :who, content: :orchid),
  iex>            Clued.CluedoFSM.card(type: :weapon, content: :candlestick),
  iex>            Clued.CluedoFSM.card(type: :who, content: :scarlet)
  iex>          ]),
  iex>       plum: Clued.CluedoFSM.player(
  iex>          name: "Professor Plum",
  iex>          location: :study,
  iex>          pid: :pid_plum,
  iex>          cards: [
  iex>            Clued.CluedoFSM.card(type: :room, content: :study),
  iex>            Clued.CluedoFSM.card(type: :who, content: :peacock),
  iex>            Clued.CluedoFSM.card(type: :weapon, content: :dagger)
  iex>          ])
  iex>     }
  iex> ))
  {:next_state, :playing_hall,
    Clued.CluedoFSM.game_state(
      active: :pid_scarlet,
      last_spin: 6,
      can_start: true,
      player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
      floorplan: [
        Clued.CluedoFSM.cell(location: :a1, entrance: :kitchen),
        Clued.CluedoFSM.cell(location: :a2),
        Clued.CluedoFSM.cell(location: :a3),
        Clued.CluedoFSM.cell(location: :a4, entrance: :study),
        Clued.CluedoFSM.cell(location: :a5),
        Clued.CluedoFSM.cell(location: :a6),
        Clued.CluedoFSM.cell(location: :a7)
      ],
      players: %{
        scarlet: Clued.CluedoFSM.player(
          name: "Miss Scarlett",
          last_location: :kitchen,
          location: :a2,
          pid: :pid_scarlet,
          cards: [
            Clued.CluedoFSM.card(type: :who, content: :orchid),
            Clued.CluedoFSM.card(type: :weapon, content: :candlestick),
            Clued.CluedoFSM.card(type: :who, content: :scarlet)
          ]),
        plum: Clued.CluedoFSM.player(
          name: "Professor Plum",
          location: :study,
          pid: :pid_plum,
          cards: [
            Clued.CluedoFSM.card(type: :room, content: :study),
            Clued.CluedoFSM.card(type: :who, content: :peacock),
            Clued.CluedoFSM.card(type: :weapon, content: :dagger)
          ])
      }
    ), [{:reply, {:pid_scarlet, 1}, :ok}]}

  iex> :rand.seed(:exsplus, {1, 2, 3})
  iex> floorplan = [
  iex>   Clued.CluedoFSM.cell(location: :a1, entrance: :kitchen),
  iex>   Clued.CluedoFSM.cell(location: :a2),
  iex>   Clued.CluedoFSM.cell(location: :a3),
  iex>   Clued.CluedoFSM.cell(location: :a4, entrance: :study),
  iex>   Clued.CluedoFSM.cell(location: :a5),
  iex>   Clued.CluedoFSM.cell(location: :a6),
  iex>   Clued.CluedoFSM.cell(location: :a7)
  iex> ]
  iex> Clued.CluedoFSM.playing_room({:call, {:pid_scarlet, 1}}, {:conclude, :plum, :rope, :kitchen},
  iex>   Clued.CluedoFSM.game_state(
  iex>     active: :pid_scarlet,
  iex>     last_spin: 6,
  iex>     can_start: true,
  iex>     player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
  iex>     floorplan: floorplan,
  iex>     players: %{
  iex>       scarlet: Clued.CluedoFSM.player(
  iex>          name: "Miss Scarlett",
  iex>          location: :kitchen,
  iex>          pid: :pid_scarlet,
  iex>          cards: [
  iex>            Clued.CluedoFSM.card(type: :who, content: :orchid),
  iex>            Clued.CluedoFSM.card(type: :weapon, content: :candlestick),
  iex>            Clued.CluedoFSM.card(type: :who, content: :scarlet)
  iex>          ]),
  iex>       plum: Clued.CluedoFSM.player(
  iex>          name: "Professor Plum",
  iex>          location: :study,
  iex>          pid: :pid_plum,
  iex>          cards: [
  iex>            Clued.CluedoFSM.card(type: :room, content: :study),
  iex>            Clued.CluedoFSM.card(type: :who, content: :peacock),
  iex>            Clued.CluedoFSM.card(type: :weapon, content: :dagger)
  iex>          ])
  iex>     },
  iex>     room: :study,
  iex>     guess: Clued.CluedoFSM.guess(who: :plum, what: :rope, where: :kitchen),
  iex>     crime: Clued.CluedoFSM.crime(who: :plum, what: :rope, where: :kitchen)
  iex> ))
  {:next_state, :game_over,
    Clued.CluedoFSM.game_state(
      active: :pid_scarlet,
      winner: :pid_scarlet,
      last_spin: 6,
      can_start: true,
      player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
      floorplan: [
        Clued.CluedoFSM.cell(location: :a1, entrance: :kitchen),
        Clued.CluedoFSM.cell(location: :a2),
        Clued.CluedoFSM.cell(location: :a3),
        Clued.CluedoFSM.cell(location: :a4, entrance: :study),
        Clued.CluedoFSM.cell(location: :a5),
        Clued.CluedoFSM.cell(location: :a6),
        Clued.CluedoFSM.cell(location: :a7)
      ],
      players: %{
        scarlet: Clued.CluedoFSM.player(
          name: "Miss Scarlett",
          location: :kitchen,
          pid: :pid_scarlet,
          cards: [
            Clued.CluedoFSM.card(type: :who, content: :orchid),
            Clued.CluedoFSM.card(type: :weapon, content: :candlestick),
            Clued.CluedoFSM.card(type: :who, content: :scarlet)
          ]),
          plum: Clued.CluedoFSM.player(
            name: "Professor Plum",
            location: :study,
            pid: :pid_plum,
            cards: [
              Clued.CluedoFSM.card(type: :room, content: :study),
              Clued.CluedoFSM.card(type: :who, content: :peacock),
              Clued.CluedoFSM.card(type: :weapon, content: :dagger)
            ])
      },
      room: :study,
      guess: Clued.CluedoFSM.guess(who: :plum, what: :rope, where: :kitchen),
      crime: Clued.CluedoFSM.crime(who: :plum, what: :rope, where: :kitchen)
    ), [{:reply, {:pid_scarlet, 1}, :ok}]}

  iex> IO.puts "Conclude"
  iex> :rand.seed(:exsplus, {1, 2, 3})
  iex> floorplan = [
  iex>   Clued.CluedoFSM.cell(location: :a1, entrance: :kitchen),
  iex>   Clued.CluedoFSM.cell(location: :a2),
  iex>   Clued.CluedoFSM.cell(location: :a3),
  iex>   Clued.CluedoFSM.cell(location: :a4, entrance: :study),
  iex>   Clued.CluedoFSM.cell(location: :a5),
  iex>   Clued.CluedoFSM.cell(location: :a6),
  iex>   Clued.CluedoFSM.cell(location: :a7)
  iex> ]
  iex> Clued.CluedoFSM.playing_room({:call, {:pid_scarlet, 1}}, {:conclude, :plum, :resolver, :kitchen},
  iex>   Clued.CluedoFSM.game_state(
  iex>     active: :pid_scarlet,
  iex>     last_spin: 6,
  iex>     can_start: true,
  iex>     player_order: [{:pid_plum, 7}, {:pid_scarlet, 6}],
  iex>     floorplan: floorplan,
  iex>     players: %{
  iex>       scarlet: Clued.CluedoFSM.player(
  iex>          name: "Miss Scarlett",
  iex>          location: :kitchen,
  iex>          pid: :pid_scarlet,
  iex>          cards: [
  iex>            Clued.CluedoFSM.card(type: :who, content: :orchid),
  iex>            Clued.CluedoFSM.card(type: :weapon, content: :candlestick),
  iex>            Clued.CluedoFSM.card(type: :who, content: :scarlet)
  iex>          ]),
  iex>       plum: Clued.CluedoFSM.player(
  iex>          name: "Professor Plum",
  iex>          location: :study,
  iex>          pid: :pid_plum,
  iex>          cards: [
  iex>            Clued.CluedoFSM.card(type: :room, content: :study),
  iex>            Clued.CluedoFSM.card(type: :who, content: :peacock),
  iex>            Clued.CluedoFSM.card(type: :weapon, content: :dagger)
  iex>          ])
  iex>     },
  iex>     room: :study,
  iex>     guess: Clued.CluedoFSM.guess(who: :plum, what: :rope, where: :kitchen),
  iex>     crime: Clued.CluedoFSM.crime(who: :plum, what: :rope, where: :kitchen)
  iex> ))
  {:next_state, :playing_room,
    Clued.CluedoFSM.game_state(
      active: :pid_plum,
      last_spin: nil,
      can_start: true,
      player_order: [{:pid_plum, 7}],
      floorplan: [
        Clued.CluedoFSM.cell(location: :a1, entrance: :kitchen),
        Clued.CluedoFSM.cell(location: :a2),
        Clued.CluedoFSM.cell(location: :a3),
        Clued.CluedoFSM.cell(location: :a4, entrance: :study),
        Clued.CluedoFSM.cell(location: :a5),
        Clued.CluedoFSM.cell(location: :a6),
        Clued.CluedoFSM.cell(location: :a7)
      ],
      players: %{
        scarlet: Clued.CluedoFSM.player(
          name: "Miss Scarlett",
          location: :kitchen,
          pid: :pid_scarlet,
          done: true,
          cards: [
            Clued.CluedoFSM.card(type: :who, content: :orchid),
            Clued.CluedoFSM.card(type: :weapon, content: :candlestick),
            Clued.CluedoFSM.card(type: :who, content: :scarlet)
          ]),
        plum: Clued.CluedoFSM.player(
          name: "Professor Plum",
          location: :study,
          pid: :pid_plum,
          cards: [
            Clued.CluedoFSM.card(type: :room, content: :study),
            Clued.CluedoFSM.card(type: :who, content: :peacock),
            Clued.CluedoFSM.card(type: :weapon, content: :dagger)
          ]
        )
      },
      room: :study,
      guess: nil,
      crime: Clued.CluedoFSM.crime(who: :plum, what: :rope, where: :kitchen)
    ), [{:reply, {:pid_scarlet, 1}, :ok}]}

  """
  def playing_room({:call, from_ref}, {:guess, suspect, weapon, scene}, state)
  when game_state(state, :guess) == nil
  do
    {player_pid, _} = from_ref
    {last_location, location} = Enum.find_value(game_state(state, :players), fn {_role, p} ->
      if player(p, :pid) == player_pid, do: {player(p, :last_location), player(p, :location)}
    end)

    cond do
      player_pid != game_state(state, :active) ->
        Clued.CliClient.tell(player_pid, "You are not the active player")
        {:next_state, :playing_room, state, [{:reply, from_ref, {:error, :badcall}}]}
      scene != location ->
        Clued.CliClient.tell(player_pid, "You cannot guess room other than room #{location}")
        {:next_state, :playing_room, state, [{:reply, from_ref, {:error, :badcall}}]}
      is_valid_room(last_location) && last_location == scene ->
        {:next_state, :playing_room, state, [{:reply, from_ref, {:error, :badcall}}]}
      true ->
        Logger.info "Making a guess"
        suspect_player = Map.get(game_state(state, :players), suspect)
        new_players = Map.replace!(game_state(state, :players), suspect,
          player(suspect_player, location: scene, last_location: scene))
        new_game_state = game_state(state, guess: guess(who: suspect,
                                                        what: weapon,
                                                        where: scene),
                                           players: new_players)
        case player(suspect_player, :pid) do
          nil -> :ok
          suspect_player_pid ->
            Clued.CliClient.tell(suspect_player_pid, "You are being moved to #{scene}")
        end

        who_has_cards = game_state(state, :players)
          # Collect the cards
          |> Enum.reduce([], fn {role, p}, acc ->
              cards = player(p, :cards)
                |> Enum.filter(fn c -> card(c, :content) in [suspect, weapon, scene] end)
                |> Enum.map(&(card(&1, :content)))
              # IO.inspect cards, label: "cards"
              if length(cards) == 0 do
                acc
              else
                acc ++ [{player(p, :pid), role, cards}]
              end
            end)
        who_has_cards
          # Dispatch message to card owners and player
          |> Enum.each(fn {card_owner_pid, role, cards} ->
            Logger.info "Player [#{role}] has cards"
            Logger.debug inspect([cards: cards])

            if card_owner_pid == player_pid do
              Clued.CliClient.tell(player_pid, "You [#{role}] have cards #{Enum.join(cards, ", ")}")
            else
              Clued.CliClient.tell(player_pid, "Player [#{role}] has cards #{Enum.join(cards, ", ")}")
            end

            if is_pid(card_owner_pid) and card_owner_pid != player_pid do
              Clued.CliClient.tell(card_owner_pid, "You are showing your cards to active player")
            else
              Logger.warn "Cards belong(s) to no actual players"
            end
          end)

        whos = who_has_cards |> Enum.map(fn {_, role, _} -> role end)
        broadcast_msg = "Players #{Enum.join(whos, ", ")} are secretly presenting cards..."

        Logger.debug inspect([broadcast_msg: broadcast_msg])
        game_state(state, :players)
          |> Enum.each(fn
            {_role, p} when player(p, :pid) == nil -> :ok
            {_role, p} when player(p, :pid) == player_pid -> :ok
            {_role, p} ->
              recipient_pid = player(p, :pid)
              Logger.debug inspect([recipient_pid: recipient_pid])

              case List.keyfind(who_has_cards, recipient_pid, 0) do
                nil -> Clued.CliClient.tell(recipient_pid, broadcast_msg)
                _ -> :ok
              end
          end)

        {:next_state, :playing_room, new_game_state, [{:reply, from_ref, :ok}]}
    end
  end

  def playing_room({:call, from_ref}, {:spin}, state)
  when game_state(state, :last_spin) != nil do # Prevent re-entrance
    Logger.warn "Subsequent call(s) to spin"
    {player_pid, _}  = from_ref
    Clued.CliClient.tell(player_pid, "You have already rolled the dice")
    {:keep_state, state, [{:reply, from_ref, {:error, :badcall}}]}
  end
  def playing_room({:call, from_ref}, {:spin}, state)
  when elem(from_ref, 0) != game_state(state, :active) do
    Logger.warn "Not the active player"
    {player_pid, _}  = from_ref
    Clued.CliClient.tell(player_pid, "You are not the active player")
    {:keep_state, state, [{:reply, from_ref, {:error, :badcall}}]}
  end
  def playing_room({:call, from_ref}, {:spin}, state) do
    {player_pid, _} = from_ref
    Logger.debug inspect([player_pid: player_pid])
    dice = spin_dice()
    Clued.CliClient.tell(player_pid, "You've rolled #{dice}")

    Logger.debug inspect([dice: dice])
    new_game_state = game_state(state, last_spin: dice)

    {:next_state, :playing_room, new_game_state, [{:reply, from_ref, {:ok, dice}}]}
  end

  def playing_room({:call, from_ref}, {:pass}, state) do
    {player_pid, _} = from_ref
    if player_pid != game_state(state, :active) do
      {:next_state, :playing_room, state, [{:reply, from_ref, {:error, :badcall}}]}
    else
      next_active = round_robin(game_state(state, :player_order), player_pid)
      role = Enum.find_value(game_state(state, :players), fn {role, p} ->
        case player(p, :pid) do
           p_pid when p_pid == next_active -> role
           _ -> nil
        end
      end)

      Logger.info "Passing to the next player #{role}"
      Clued.CliClient.tell(player_pid, "Passing to the next player #{role}")
      {:next_state, :playing_room, game_state(state,
        active: next_active,
        guess: nil,
        last_spin: nil),
        [{:reply, from_ref, :ok}]
      }
    end
  end

  def playing_room({:call, from_ref}, {:move, location}, state) when is_bitstring(location) do
    {player_pid, _} = from_ref
    floorplan = game_state(state, :floorplan)

    if !is_valid_location(floorplan, location) do
      Clued.CliClient.tell(player_pid, "Cannot move! #{location} is invalid")
      {:next_state, :playing_room, state, [{:reply, from_ref, {:error, :badcall}}]}
    else
      playing_hall({:call, from_ref}, {:move, String.to_atom(location)}, state)
    end
  end
  def playing_room({:call, from_ref}, {:move, location}, state) when is_atom(location) do
    {player_pid, _} = from_ref
    {role, p} = Enum.find(game_state(state, :players), nil, fn({_key, it}) ->
        player(it, :pid) == player_pid
    end)
    if p == nil do
      {:next_state, :playing_room, state, [{:reply, from_ref, {:error, :badcall}}]}
    else
      case move_to_location(state, p, role, location) do
        {:error, err} ->
          {:next_state, :playing_room, state, [{:reply, from_ref, {:error, err}}]}
        {:playing_hall, new_game_state} ->
          {:next_state, :playing_hall, new_game_state, [{:reply, from_ref, :ok}]}
        {:playing_room, new_game_state} ->
          {:next_state, :playing_room, new_game_state, [{:reply, from_ref, :ok}]}
      end
    end
  end

  def playing_room({:call, from_ref}, {:conclude, suspect, weapon, scene}, state) do
    {player_pid, _} = from_ref
    if player_pid != game_state(state, :active) do
      {:next_state, :playing_room, state, [{:reply, from_ref, {:error, :badcall}}]}
    else
      Logger.info "Making a conclusion"

      {:crime, crime_suspect, crime_weapon, crime_scene} = game_state(state, :crime)
      if crime_suspect == suspect && crime_weapon == weapon && crime_scene == scene do
        Clued.CliClient.tell(player_pid, "You win!")
        new_game_state = game_state(state, winner: player_pid)

        {:next_state, :game_over, new_game_state, [{:reply, from_ref, :ok}]}
      else
        Clued.CliClient.tell(player_pid, "You lose and now have become passive!")
        {role, p} = Enum.find(game_state(state, :players), nil, fn({_key, it}) ->
            player(it, :pid) == player_pid
        end)
        players = game_state(state, :players)

        new_active = round_robin(game_state(state, :player_order), player_pid)
        new_players = Map.replace!(players, role, player(p, done: true))
        new_player_order = Enum.filter(game_state(state, :player_order), fn ({it_pid, _}) -> it_pid != player_pid end)

        if length(new_player_order) == 0 do
          new_game_state = game_state(state, winner: nil)
          {:next_state, :game_over, new_game_state, [{:reply, from_ref, :ok}]}
        else
          {next_role, next_player} = Enum.find_value(game_state(state, :players), fn {role, p} ->
            if player(p, :pid) == new_active do
              {role, p}
            else
              nil
            end
          end)
          Logger.info "Active role will be #{next_role}"
          Clued.CliClient.tell(player_pid, "Active role will be #{next_role}")

          new_game_state = game_state(state,
            active: new_active,
            guess: nil,
            last_spin: nil,
            players: new_players,
            player_order: new_player_order)
          # If the next player is in a room, let him be in a room
          new_statename = if is_room(player(next_player, :location)) do
            :playing_room
          else
            :playing_hall
          end
          {:next_state, new_statename, new_game_state, [{:reply, from_ref, :ok}]}
        end
      end
    end
  end
  def playing_room(event_type, content, state) do
    handle_common(event_type, content, state)
  end

  def move_to_location(state, this_player, role, new_location) do
    player_pid = player(this_player, :pid)

    Logger.info "[#{role}] Making a move to new location"
    Clued.CliClient.tell(player_pid, "Moving to new location #{new_location}")
    floorplan = game_state(state, :floorplan)

    if !is_valid_location(floorplan, new_location) do
      # {:next_state, :playing_hall, state, [{:reply, from_ref, {:error, :badcall}}]}
      {:error, :badcall}
    else
      player_location = player(this_player, :location)
      if player_location == :kitchen do
        Logger.debug inspect([player_location: player_location,
                              new_location: new_location])
      end
      if distance(floorplan, player_location, new_location) > game_state(state, :last_spin) do
        {:error, :badcall}
      else
        last_location = player(this_player, :location)
        case List.keyfind(floorplan, new_location, cell(:location)) do
          c when cell(c, :entrance) == nil ->
            new_players = Map.replace!(game_state(state, :players), role,
              player(this_player, location: new_location, last_location: last_location))
            new_game_state = game_state(state, players: new_players)
            {:playing_hall, new_game_state}
          c ->
            room = cell(c, :entrance)
            Logger.info "[#{role}] Player has entered room #{room}"
            Clued.CliClient.tell(player_pid, "You have entered room #{room}")

            new_players = Map.replace!(game_state(state, :players), role,
              player(this_player, location: room, last_location: last_location))
            new_game_state = game_state(state,
              players: new_players,
              room: room)
            {:playing_room, new_game_state}
        end
      end
    end
  end

  @doc """
  Given the number of player suffices, returns true

  ## Examples

  iex> players = %{
  iex>   scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :study, pid: :pid_scarlet),
  iex>   plum: Clued.CluedoFSM.player(name: "Professor Plum", location: :unknown)
  iex> }
  iex> Clued.CluedoFSM.is_enough_players(players)
  false

  iex> players = %{
  iex>   scarlet: Clued.CluedoFSM.player(name: "Miss Scarlett", location: :study, pid: :pid_scarlet),
  iex>   plum: Clued.CluedoFSM.player(name: "Professor Plum", location: :unknown, pid: :pid_plum)
  iex> }
  iex> Clued.CluedoFSM.is_enough_players(players)
  true

  """
  def is_enough_players(players) do
    length(Enum.filter(players, fn ({_role, player(pid: it_pid)}) -> it_pid != nil end)) >= 2
  end

  def generate_crime(crime_cards) do
    suspect = card(Enum.at(crime_cards, 0), :content)
    weapon = card(Enum.at(crime_cards, 1), :content)
    scene = card(Enum.at(crime_cards, 2), :content)
    crime(who: suspect, what: weapon, where: scene)
  end

  @doc """
  Deal the deck to n players
  Cards in deck are sorted

  ## Examples

  iex> :rand.seed(:exsplus, {1, 2, 3})
  iex> Clued.CluedoFSM.deal_deck(3)
  [
    [
      {:card, :who, :mustard},
      {:card, :weapon, :leadpipe},
      {:card, :room, :study}
    ],
    [
      {:card, :room, :billard},
      {:card, :weapon, :revolver},
      {:card, :who, :green}
    ],
    [
      {:card, :room, :dining},
      {:card, :room, :conservatory},
      {:card, :weapon, :dagger}
    ]
  ]

  """
  def deal_deck(n) when n <= 7 do
    [who_card | who_deck] = Enum.shuffle([
      card(type: :who, content: :scarlet),
      card(type: :who, content: :plum),
      card(type: :who, content: :peacock),
      card(type: :who, content: :green),
      card(type: :who, content: :mustard),
      card(type: :who, content: :orchid)
    ])
    [weapon_card | weapon_deck] = Enum.shuffle([
      card(type: :weapon, content: :candlestick),
      card(type: :weapon, content: :dagger),
      card(type: :weapon, content: :leadpipe),
      card(type: :weapon, content: :revolver),
      card(type: :weapon, content: :rope),
      card(type: :weapon, content: :spanner)
    ])
    [room_card | room_deck] = Enum.shuffle([
      card(type: :room, content: :kitchen),
      card(type: :room, content: :ballroom),
      card(type: :room, content: :conservatory),
      card(type: :room, content: :billard),
      card(type: :room, content: :library),
      card(type: :room, content: :study),
      card(type: :room, content: :hall),
      card(type: :room, content: :lounge),
      card(type: :room, content: :dining)
    ])

    shuffled = Enum.shuffle(who_deck ++ weapon_deck ++ room_deck)

    [[who_card, weapon_card, room_card]] ++
      Enum.reduce(0..n-2, [], fn index, acc ->
        acc ++ [[
          Enum.at(shuffled, index * 3),
          Enum.at(shuffled, index * 3 + 1),
          Enum.at(shuffled, index * 3 + 2)
        ]]
      end)
  end

  def spin_dice, do: :rand.uniform(12)

  @doc """
  Given a floorplan and a location, determines whether such a location is valid

  iex> Clued.CluedoFSM.is_valid_location([
  iex>  Clued.CluedoFSM.cell(location: :a1),
  iex>  Clued.CluedoFSM.cell(location: :a2)
  iex> ], "a1")
  true

  iex> Clued.CluedoFSM.is_valid_location([
  iex>  Clued.CluedoFSM.cell(location: :a1),
  iex>  Clued.CluedoFSM.cell(location: :a2)
  iex> ], "a2")
  true

  iex> Clued.CluedoFSM.is_valid_location([
  iex>  Clued.CluedoFSM.cell(location: :a1),
  iex>  Clued.CluedoFSM.cell(location: :a2)
  iex> ], :a1)
  true

  iex> Clued.CluedoFSM.is_valid_location([
  iex>  Clued.CluedoFSM.cell(location: :a1),
  iex>  Clued.CluedoFSM.cell(location: :a2)
  iex> ], :a3)
  false

  """
  def is_valid_location(floorplan, location) when is_atom(location) do
    List.keyfind(floorplan, location, cell(:location)) != nil
  end
  def is_valid_location(floorplan, location) when is_bitstring(location) do
    is_valid_location(floorplan, String.to_atom(location))
  end
  def is_room(location) when is_valid_room(location), do: true
  def is_room(_location), do: false

  @doc """
  Given a floorplan and two locations

  ## Examples
  iex> floorplan = [
  iex>   Clued.CluedoFSM.cell(location: :a1),
  iex>   Clued.CluedoFSM.cell(location: :a2),
  iex>   Clued.CluedoFSM.cell(location: :a3),
  iex>   Clued.CluedoFSM.cell(location: :a4),
  iex>   Clued.CluedoFSM.cell(location: :a5),
  iex>   Clued.CluedoFSM.cell(location: :a6),
  iex>   Clued.CluedoFSM.cell(location: :a7)
  iex> ]
  iex> Clued.CluedoFSM.distance(floorplan, :a1, :a3)
  2

  iex> floorplan = [
  iex>   Clued.CluedoFSM.cell(location: :a1),
  iex>   Clued.CluedoFSM.cell(location: :a2),
  iex>   Clued.CluedoFSM.cell(location: :a3),
  iex>   Clued.CluedoFSM.cell(location: :a4),
  iex>   Clued.CluedoFSM.cell(location: :a5),
  iex>   Clued.CluedoFSM.cell(location: :a6),
  iex>   Clued.CluedoFSM.cell(location: :a7)
  iex> ]
  iex> Clued.CluedoFSM.distance(floorplan, :a1, :a7)
  1

  iex> floorplan = [
  iex>   Clued.CluedoFSM.cell(location: :a1, entrance: :kitchen),
  iex>   Clued.CluedoFSM.cell(location: :a2),
  iex>   Clued.CluedoFSM.cell(location: :a3),
  iex>   Clued.CluedoFSM.cell(location: :a4),
  iex>   Clued.CluedoFSM.cell(location: :a5, entrance: :study),
  iex>   Clued.CluedoFSM.cell(location: :a6),
  iex>   Clued.CluedoFSM.cell(location: :a7)
  iex> ]
  iex> Clued.CluedoFSM.distance(floorplan, :kitchen, :a4)
  4
  """
  def distance(floorplan, pointA, pointB) do
    # circular route for a circular floorplan
    indexA = if is_room(pointA) do
      Enum.find_index(floorplan, fn it -> cell(it, :entrance) == pointA end)
    else
      Enum.find_index(floorplan, fn it -> cell(it, :location) == pointA end)
    end

    count = length(floorplan)
    indexB = Enum.find_index(floorplan, fn it -> cell(it, :location) == pointB end)

    # Add 1 unit of extra step if coming froma a room
    extra = if is_room(pointA), do: 1, else: 0

    if abs(indexA - indexB) > count/2 do
      count - abs(indexA - indexB) + extra
    else
      abs(indexA - indexB) + extra
    end
  end

  @doc """
  Given list of tuple {pid, dice} and the current pid, returns the next pid

  ## Examples

  iex> Clued.CluedoFSM.round_robin([{:pid1, 4}, {:pid2, 3}, {:pid2, 1}], :pid1)
  :pid2

  iex> Clued.CluedoFSM.round_robin([{:pid1, 4}, {:pid2, 3}, {:pid3, 1}], :pid2)
  :pid3

  iex> Clued.CluedoFSM.round_robin([{:pid1, 4}, {:pid2, 3}, {:pid3, 1}], :pid3)
  :pid1

  """
  def round_robin(player_order, current_pid) do
    index = Enum.find_index(player_order, fn ({it_pid, _}) -> it_pid == current_pid end)
    if index < length(player_order) - 1 do
      elem(Enum.at(player_order, index + 1), 0)
    else
      elem(Enum.at(player_order, 0), 0)
    end
  end
end
